package program;

import ui.*;

public class FinanceUIDemo {
	
	public static void main(String[] args) {
		FinanceGUI gui = new FinanceGUI();
	
		gui.setSize(400, 300);
		gui.setTitle("Finance App");
		gui.setVisible(true);
	}
	
}

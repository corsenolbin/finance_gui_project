package persistence;

import java.sql.*;
import java.util.UUID;
import contract.*;


public class FinanceStore implements IFinanceStore 
{
	  Connection con;
	  String url = "jdbc:sqlserver://localhost";
	  String dbName = "Finance";
	  String driver = "com.mysql.jdbc.Driver";
	  String userName = "sa"; 
	  String password = "abc";
	  
//	  String GetConnectionStr() {
//		  return 
//	  }
	  
	  public String Connect(SqlConnection connectionStr) {
		// Load the SQLServerDriver class, build the
	        // connection string, and get a connection
		  try {
	        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	        
	        con = DriverManager.getConnection(connectionStr.getConnectionUrl());
	        return "Connected.";
		  } catch (Exception e) {
			  return "Error connecting to DB.";
		  }
	  }
	  
	  public String PostPurchase(Purchase trans ){
		  try 
	      {
		
		        // Create and execute an SQL statement that returns some data. 
		        //String SQL = "SELECT * FROM [dbo].[Transaction]"; 
		        String INSERT = "INSERT INTO [dbo].[Purchase] "
		        		+ 					"([Id]"
		        		+ 					",[Amount]"
		        		+ 					",[PurchaseCode])"
		        		+ 			"VALUES  (?, ?, ?)";
		        PreparedStatement stmt = con.prepareStatement(INSERT); 
		        stmt.setObject(1,UUID.randomUUID());
		        stmt.setDouble(2, trans.money);
		        stmt.setString(3, trans.PurchaseCode);
		        stmt.execute();
		
		        // Iterate through the data in the result set and display it. 
		        
	            return "POST was successful!";
	       } 
	       catch(Exception e) 
	       {	    	    
	            return e.toString();
	       } 
	  }
	
}

package persistence;

import contract.*;

public interface IFinanceStore {
	public String PostPurchase(Purchase tran);
	public String Connect(SqlConnection con);
}

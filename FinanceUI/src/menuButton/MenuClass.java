package menuButton;

import java.awt.event.*;
import java.awt.*;
import DialogBox.*;
import ui.*;

public class MenuClass extends MenuBar{
	
	String msg = "";
	private CheckboxMenuItem debug, test;
	private Menu file, edit;
	private MenuItem item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12;
	public FinanceGUI frame; 
	
	public MenuClass(FinanceGUI frame) {
		this.frame = frame;
		
	}
	
	public void paint(Graphics g) {
		g.drawString(msg, 10, 200);
		
		g.drawString(String.format("Debug is %s.", debug.getState() ? "on" : "off"), 10, 220);
		
		if(test.getState())
			g.drawString("Testing is on.", 10, 240);
		else
			g.drawString("Testing is off.", 10, 240);
	}


	


	public void initMenu(MenuBar mbar) {
		// create menu bar and add it to frame
		
		// create the menu items
		file = new Menu("File");
		file.add(item1= new MenuItem("New..."));
		file.add(item2 = new MenuItem("Open..."));
		file.add(item3 = new MenuItem("Connect"));
		file.add(item4 = new MenuItem("-"));
		file.add(item5 = new MenuItem("Quit..."));
		mbar.add(file);
		
		edit = new Menu("Edit");
		edit.add(item6 = new MenuItem("Cut"));
		edit.add(item7 = new MenuItem("Copy"));
		edit.add(item8 = new MenuItem("Paste"));
		edit.add(item9 = new MenuItem("-"));

		
		Menu sub = new Menu("Special");
		sub.add(item10 = new MenuItem("First"));
		sub.add(item11 = new MenuItem("Second"));
		sub.add(item12 = new MenuItem("Third"));
		edit.add(sub);
		
		// this are checkable menu items
		debug = new CheckboxMenuItem("Debug");
		edit.add(debug);
		edit.add(test = new CheckboxMenuItem("Testing"));

		mbar.add(edit);
		
		MyMenuHandler handler = new MyMenuHandler(frame);
		item1.addActionListener(handler);
		item2.addActionListener(handler);
		item3.addActionListener(handler);
		item4.addActionListener(handler);
		item5.addActionListener(handler);
		item6.addActionListener(handler);
		item7.addActionListener(handler);
		item8.addActionListener(handler);
		item9.addActionListener(handler);
		item10.addActionListener(handler);
		item11.addActionListener(handler);
		item12.addActionListener(handler);
		debug.addItemListener(handler);
		test.addItemListener(handler);
		//addWindowListener(new MyMenuAdapter(this));
		
	}
}

class MyWindowAdapter extends WindowAdapter{
	public void windowClosing(WindowEvent we) {
		System.exit(0);
	}
}


class MyMenuHandler implements ActionListener, ItemListener {
	FinanceGUI menuFrame;
	
	public MyMenuHandler(FinanceGUI frame) {
		this.menuFrame = frame;
	}
	
	// Handle action events.
	public void actionPerformed(ActionEvent ae) {
		String msg = "You selected ";
		String arg = ae.getActionCommand();
		if(arg.equals("New...")) {
			msg += "New.";
			SampleDialog d = new SampleDialog(menuFrame, "New Dialog Box");
			d.initBasic();
			d.setVisible(true);
		}			
		else if(arg.equals("Open..."))
			msg += "Open.";
		else if(arg.equals("Connect")) {
			msg += "Connect.";
			SampleDialog d = new SampleDialog(menuFrame, "Connect to Database");
			d.initSQLConnect();
			d.setVisible(true);
		}
		else if(arg.equals("Quit...")) {
			msg += "Quit.";
			System.exit(0);
		}			
		else if(arg.equals("Edit..."))
			msg += "Edit.";
		else if(arg.equals("Cut"))
			msg += "Cut.";
		else if(arg.equals("Copy"))
			msg += "Copy.";
		else if(arg.equals("Paste"))
			msg += "Paste.";
		else if(arg.equals("First"))
			msg += "First.";
		else if(arg.equals("Second"))
			msg += "Second.";
		else if(arg.equals("Third"))
			msg += "Third.";
		else if(arg.equals("Debug"))
			msg += "Debug.";
		else if(arg.equals("Testing"))
			msg += "Testing.";
		
		menuFrame.msg = msg;
		menuFrame.repaint();
	}
	
	public void itemStateChanged(ItemEvent ie) {
		menuFrame.repaint();
	}
}



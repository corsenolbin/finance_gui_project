package DialogBox;

import java.awt.*;
import java.awt.event.*;

import contract.SqlConnection;
import eventlisteners.*;
import persistence.*;

public class SampleDialog extends Dialog implements ActionListener{
	Label connectionAddressl, DBNamel, userNamel, passwordl;
	TextField connectionAddress, DBName, userName, password;
	Button connect;
	String str;
	
	public SampleDialog(Frame parent, String title){
		super(parent, title, false);
		setLayout(new FlowLayout());
		setSize(300,200);
		
		
	}
	
	public void initBasic() {
		add(new Label("Press this button: "));
		Button b;
		add(b = new Button("Cancel"));
		str = "This a dialog box.";
		b.addActionListener(this);
		addWindowListener(new MyDialogAdapter(this));
	}
	
	public void initSQLConnect() {
		
		
		setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		add(connectionAddressl = new Label("Connection Address: "));
		add(connectionAddress = new TextField(12));
		add(DBNamel = new Label("Database Name: "));
		add(DBName = new TextField(12));
		add(userNamel = new Label("UserName: "));
		add(userName = new TextField(12));
		add(passwordl = new Label("Password: "));
		add(password = new TextField(12));
		add(connect = new Button());
		
		connect.addActionListener(new SQLConnectActionListener(this));
		addWindowListener(new MyDialogAdapter(this));
	}
	
	public void actionPerformed(ActionEvent ae) {
		dispose();
	}
	
	public void paint(Graphics g) {
		g.drawString("Message: " + str, 10, 70);
	}
	
}

class SQLConnectActionListener implements ActionListener {
	SampleDialog dialog;
	
	public SQLConnectActionListener(SampleDialog dialog) {
		this.dialog = dialog;
	}
	
	public void actionPerformed(ActionEvent e) {
		SqlConnection sqlCon = new SqlConnection();
		sqlCon.setConnectionAddress(dialog.connectionAddress.getText());
		sqlCon.setDatabaseName(dialog.DBName.getText());
		sqlCon.setUserName(dialog.userName.getText());
		sqlCon.setPassword(dialog.password.getText());
		IFinanceStore _store = new FinanceStore();
		dialog.str = _store.Connect(sqlCon);
		dialog.repaint();
	}
	
}

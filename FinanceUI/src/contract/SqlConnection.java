package contract;

public class SqlConnection {
	private String DriverClass;
	private String connectionAddress;
	private String databaseName;
	private String userName;
	private String password;
	
	public String getConnectionUrl() {
		return connectionAddress + 
				";database=" + databaseName + 
				";user=" + userName + 
				";password=" + password;
	}
	
	public void setConnectionAddress(String connection) {
		connectionAddress = connection;
	}
	
	public void setDatabaseName(String name) {
		databaseName = name;
	}
	
	public void setUserName(String user) {
		userName = user;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
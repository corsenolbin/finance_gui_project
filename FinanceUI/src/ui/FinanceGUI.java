package ui;


import persistence.*;
import java.awt.*;
import java.awt.event.*;
import contract.*;
import eventlisteners.*;
import menuButton.*;


public class FinanceGUI extends Frame implements ItemListener, ActionListener{
	
	//Components
	private TextField money;
	private Label moneyl;
	private Choice PurchaseCode;
	private Button executeSQL;
	
	//Other variables
	private String amountStr;
	private Double amount = 0d;	
	public String msg = "";
	private String sqlStr = "";
	private Purchase tran;
	
	public FinanceGUI() {
		
		
		addWindowListener(new MainWindowAdapter());
		
		init();
		
	}
	
	private void init() {
		tran = new Purchase();
		MenuBar menu = new MenuBar();
		MenuClass menuClass = new MenuClass(this);
		menuClass.initMenu(menu);
		setMenuBar(menu);
		
		setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		moneyl = new Label("Purchase Amount: ", Label.RIGHT);
		money = new TextField(10);
		PurchaseCode = new Choice();
		executeSQL = new Button("ADD");
		
		// add items to OS list
		PurchaseCode.add("GR");
		PurchaseCode.add("EL");
		PurchaseCode.add("WK");
		PurchaseCode.add("SCH");

		add(moneyl);
		add(money);
		add(PurchaseCode);
		add(executeSQL);
		
		money.addActionListener(this);
		PurchaseCode.addItemListener(this);
		executeSQL.addActionListener(new SQLButtonListener());
	}

	public void itemStateChanged(ItemEvent e) {
		tran.PurchaseCode = PurchaseCode.getSelectedItem();
		repaint();
	}
	
	// Display current selections.
	public void paint(Graphics g) {
		msg = "Purchase Amount: ";
		msg += amountStr;
		g.drawString(msg, 10, 120);
		msg = "Purchase Code: ";
		msg += PurchaseCode.getSelectedItem();
		g.drawString(msg, 10, 140);
		msg = "SQL Message: ";
		msg += sqlStr;
		g.drawString(msg, 10, 160);

	}

	
	public void actionPerformed(ActionEvent e) {
		try {
			amountStr = money.getText();
			amount = Double.valueOf(amountStr);
			tran.money = amount;
			
			
		} catch(Exception ex) {
			amount = 0d;
			amountStr = "Enter a number! ";
		}
		repaint();
		
	}
	
	class SQLButtonListener implements ActionListener{
		private IFinanceStore fs = new FinanceStore();

		public void actionPerformed(ActionEvent e) {
			amountStr = money.getText();
			amount = Double.valueOf(amountStr);
			if(amount == 0) {
				amountStr = "Enter a number!";
			} else {
				Purchase newTran = new Purchase();				
				newTran.money = amount;
				newTran.PurchaseCode = PurchaseCode.getSelectedItem();
				sqlStr = fs.PostPurchase(newTran);
			}
			repaint();
		}
		
	}
	
}





package eventlisteners;

import java.awt.*;
import java.awt.event.*;



public class MyDialogAdapter extends WindowAdapter{
	
	private Dialog dialog;
	
	public MyDialogAdapter(Dialog dialog){
		this.dialog = dialog;
	}
	
	public void windowClosing(WindowEvent we) {
		dialog.dispose();
	}
}
